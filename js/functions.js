(function($){
	$(document).ready(function (){
		$('.of-iconmntop').click(function(){
			$('.of-menutop').toggleClass('of-mnshow');
		});
		$('.of-search').click(function(){
			$('.of-searchbox').show(200);
		});
		$('.of-closesearch').click(function(){
			$('.of-searchbox').hide(200);
		});	
	});
})
var swiper = new Swiper('.swiper-container', {
	slidesPerView: 5,
	spaceBetween: 10,
	freeMode: true,
	pagination: {
	  el: '.swiper-pagination',
	  clickable: true,
	},
  });
  $("input.ss-rad").change(function () {
	if ($("input.ss-rad:checked").val() == "den") {
		$('.ss6-img img').attr('src', 'images/ss6.png');
	}
	else if ($("input.ss-rad:checked").val() == "xanh") {
		$('.ss6-img img').attr('src', 'images/ss1-lf.png');
	}
});

(window.jQuery)